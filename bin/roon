#!/bin/bash
#
# roon - frontend shell script to issue remote commands to Roon via SSH and the Roon API
#
#  Copyright 2021, Ronald Joe Record
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#
# Edit these two settings
#   The IP address of the system on which the Python Roon API is installed
server="XX.X.X.XXX"
#   The username that has public key authorized SSH access to the Python Roon API system
user="SSH_USERNAME"

album=
artist=
comm=
albumlist=
artistlist=
genrelist=
playlistlist=
taglist=
playlist=
radio=
search=
tag=
zone=
zonegroup=
zonelist=

usage() {
  printf "\nUsage: roon -A album -a artist -g genre -G zone_group -l [albums|artists|genres|playlists|tags|zones] -s search -p playlist -t tag -z zone\n\t-c [group|ungroup|play|pause|stop|next|previous|shuffle|unshuffle|repeat|unrepeat|mute|unmute]\n\t-L -r -u"
  printf "\n\tWhere:\n\t\t-A album selects an album to play"
  printf "\n\t\t-a artist selects an artist to play"
  printf "\n\t\t-g genre selects a genre to play"
  printf "\n\t\t-p playlist selects a playlist to play"
  printf "\n\t\t-G zone_group specifies a zone grouping specified in roon_api.ini"
  printf "\n\t\t-L setup roon to execute local commands rather than remote via SSH"
  printf "\n\t\t-l [albums|artists|genres|playlists|tags|zones] indicates list albums, artists, genres, playlists, tags, or Roon zones"
  printf "\n\t\t-s search specifies a term to search for in the lists retrieved with -l"
  printf "\n\t\t-r indicates play Radio Paradise"
  printf "\n\t\t-t tag selects an tag to play (not yet working)"
  printf "\n\t\t-z zone selects the Roon Zone in which to play"
  printf "\n\t\t-c [group|ungroup|play|pause|playpause|stop|next|previous|shuffle|unshuffle|repeat|unrepeat|mute|unmute]"
  printf "\n\t\t\tissues the command in the selected zone"
  printf "\n\n\tExample invocations"
  printf "\n\t\tPlay artist:"
  printf "\n\t\t\troon -a \"Deep Purple\""
  printf "\n\t\tPlay artist in specified zone:"
  printf "\n\t\t\troon -a \"Jethro Tull\" -z \"Living Room\""
  printf "\n\t\tPlay genre:"
  printf "\n\t\t\troon -g Classical"
  printf "\n\t\tPlay playlist:"
  printf "\n\t\t\troon -p \"Bowie Favs\""
  printf "\n\t\tPlay next track:"
  printf "\n\t\t\troon -c next"
  printf "\n\t\tStop play in specified zone:"
  printf "\n\t\t\troon -c stop -z Kitchen"
  printf "\n\t\tMute a specified zone:"
  printf "\n\t\t\troon -c mute -z \"Living Room\""
  printf "\n\t\tList all playlists containing the string 'Best':"
  printf "\n\t\t\troon -l playlists -s Best"
  printf "\n\t\tGroup the zones listed in roon_api.ini Group_foobar:"
  printf "\n\t\t\troon -G foobar -c group"
  printf "\n\t\tNOTE: Use quotes to specify media names which contain spaces."
  printf "\n\t\tFor example, to play the album 'Love Bomb':"
  printf "\n\t\t\troon -A \"Love Bomb\"\n\n"
  exit 1
}

[ "$1" ] || usage

ROON=/usr/local/Roon
ROONAPI=${ROON}/api
ROONETC=${ROON}/etc
ROONCONF=${ROONETC}/pyroonconf
LOCAL=false
[ -f ${ROONCONF} ] && . ${ROONCONF}

while getopts A:a:g:G:p:t:z:c:l:s:Lru flag; do
    case $flag in
        A)
            album="$OPTARG"
            ;;
        a)
            artist="$OPTARG"
            ;;
        g)
            genre="$OPTARG"
            ;;
        p)
            playlist="$OPTARG"
            ;;
        t)
            tag="$OPTARG"
            ;;
        z)
            zone="$OPTARG"
            ;;
        c)
            comm="$OPTARG"
            ;;
        l)
            listname="$OPTARG"
            if [ "$listname" == "albums" ] || [ "$listname" == "album" ]
            then
                albumlist=1
            elif [ "$listname" == "artists" ] || [ "$listname" == "artist" ]
            then
                artistlist=1
            elif [ "$listname" == "genres" ] || [ "$listname" == "genre" ]
            then
                genrelist=1
            elif [ "$listname" == "playlists" ] || [ "$listname" == "playlist" ]
            then
                playlistlist=1
            elif [ "$listname" == "tags" ] || [ "$listname" == "tag" ]
            then
                taglist=1
            elif [ "$listname" == "zones" ] || [ "$listname" == "zone" ]
            then
                zonelist=1
            else
                echo "Unknown list type: $listname"
                usage
            fi

            ;;
        G)
            zonegroup="$OPTARG"
            ;;
        L)
            if [ -f ${ROONCONF} ]
            then
                grep -v ^LOCAL= ${ROONCONF} > /tmp/roon$$
                echo "LOCAL=true" >> /tmp/roon$$
                cp /tmp/roon$$ ${ROONCONF}
                rm -f /tmp/roon$$
                . ${ROONCONF}
            else
                echo "${ROONCONF} does not exist"
                echo "RoonCommandLine is not installed locally"
            fi
            ;;
        r)
            radio=1
            ;;
        s)
            search="$OPTARG"
            ;;
        u)
            usage
            ;;
    esac
done

[ "$search" ] || search="all"

# Set Zone
[ "$zone" ] && {
    if [ "${LOCAL}" = true ]
    then
        set_zone $zone
    else
        ssh $user@$server "bash -l -c \"set_zone $zone\""
    fi
}

# Zone Grouping
[ "$zonegroup" ] && {
    [ "$comm" == "group" ] || [ "$comm" == "ungroup" ] || comm=group
    if [ "${LOCAL}" = true ]
    then
        set_zone_group $comm $zonegroup
    else
        ssh $user@$server "bash -l -c \"set_zone_group $comm $zonegroup\""
    fi
}

[ "$albumlist" ] && {
    if [ "${LOCAL}" = true ]
    then
        list_albums $search
    else
        ssh $user@$server "bash -l -c \"list_albums $search\""
    fi
}
[ "$artistlist" ] && {
    if [ "${LOCAL}" = true ]
    then
        list_artists $search
    else
        ssh $user@$server "bash -l -c \"list_artists $search\""
    fi
}
[ "$genrelist" ] && {
    if [ "${LOCAL}" = true ]
    then
        list_genres $search
    else
        ssh $user@$server "bash -l -c \"list_genres $search\""
    fi
}
[ "$playlistlist" ] && {
    if [ "${LOCAL}" = true ]
    then
        list_playlists $search
    else
        ssh $user@$server "bash -l -c \"list_playlists $search\""
    fi
}
[ "$taglist" ] && {
    if [ "${LOCAL}" = true ]
    then
        list_tags $search
    else
        ssh $user@$server "bash -l -c \"list_tags $search\""
    fi
}
[ "$zonelist" ] && {
    if [ "${LOCAL}" = true ]
    then
        list_zones $search
    else
        ssh $user@$server "bash -l -c \"list_zones $search\""
    fi
}
[ "$album" ] && {
    if [ "${LOCAL}" = true ]
    then
        play_album $album
    else
        ssh $user@$server "bash -l -c \"play_album $album\""
    fi
}
[ "$artist" ] && {
    if [ "${LOCAL}" = true ]
    then
        play_artist $artist
    else
        ssh $user@$server "bash -l -c \"play_artist $artist\""
    fi
}
[ "$genre" ] && {
    if [ "${LOCAL}" = true ]
    then
        play_genre $genre
    else
        ssh $user@$server "bash -l -c \"play_genre $genre\""
    fi
}
[ "$playlist" ] && {
    if [ "${LOCAL}" = true ]
    then
        play_playlist $playlist
    else
        ssh $user@$server "bash -l -c \"play_playlist $playlist\""
    fi
}
[ "$tag" ] && {
    if [ "${LOCAL}" = true ]
    then
        play_tag $tag
    else
        ssh $user@$server "bash -l -c \"play_tag $tag\""
    fi
}
[ "$radio" ] && {
    if [ "${LOCAL}" = true ]
    then
        play_rp
    else
        ssh $user@$server "bash -l -c play_rp"
    fi
}

[ "$comm" ] && {
    case "$comm" in
    "shuffle"|"unshuffle"|"repeat"|"unrepeat"|"mute"|"unmute"|"next"|"previous"|"play"|"pause"|"stop"|"playpause")
        if [ "${LOCAL}" = true ]
        then
            zone_command -c ${comm}
        else
            ssh $user@$server "bash -l -c \"zone_command -c ${comm}\""
        fi
        ;;
    "group"|"ungroup")
        ;;
    *)
        echo "Unrecognized Roon command: $comm"
        usage
        ;;
    esac
}

exit 0
