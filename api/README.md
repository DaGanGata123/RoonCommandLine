# RoonCommandLine/api

Python scripts to access the Python Roon API

## Contents

[**get_core_ip.py**](get_core_ip.py) - Python script backend to retrieve the Roon Core IP address

[**play_album.py**](play_album.py) - Python script backend for playing a specified album in my Roon library

[**play_artist.py**](play_artist.py) - Python script backend for playing a specified artist in my Roon library

[**play_genre.py**](play_genre.py) - Python script backend for playing a specified genre in my Roon library

[**play_playlist.py**](play_playlist.py) - Python script backend for playing a specified playlist in my Roon library

[**play_rp.py**](play_rp.py) - Python script backend for playing Radio Paradise Main Channel in a Roon zone

[**play_tag.py**](play_tag.py) - Python script backend for playing a specified tag in my Roon library (not yet working)

[**list_albums.py**](list_albums.py) - Python script backend for listing available Albums in your Roon Library

[**list_artists.py**](list_artists.py) - Python script backend for listing available Artists in your Roon Library

[**list_genres.py**](list_genres.py) - Python script backend for listing available Genres in your Roon Library

[**list_playlists.py**](list_playlists.py) - Python script backend for listing available Roon Playlists

[**list_tags.py**](list_tags.py) - Python script backend for listing available Roon Library tags

[**list_zones.py**](list_zones.py) - Python script backend for listing available Roon Zones

[**zone_command.py**](zone_command.py) - Python script backend for sending commands to selected Roon Zone (play, pause, mute, next track, etc)

[**zone_group.py**](zone_group.py) - Python script backend for grouping and ungrouping zones specified in roon_api.ini
